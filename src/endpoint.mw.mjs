import logger from './logger.mjs';

/** @type {import('express').RequestHandler;} */
const endpoint_mw = (req, res, next) => {
    // You can use 'req.url' to get the path of the request.
    // 'req.method' will give you the HTTP method (GET, POST, etc.).
    const url = req.url;  // The URL to which the request is made.
    const method = req.method;  // The HTTP method of the request.
    
    // Log the method and URL.
    logger.info(`[ENDPOINT] ${method} ${url}`);
    
    // Continue to the next middleware function in the stack.
    next();
};

export default endpoint_mw;
