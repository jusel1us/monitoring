import express from 'express';
import Counter from './counter.mjs';
import logger from './logger.mjs';
import endpoint_mw from './endpoint.mw.mjs';

// Counter

const app = express();
const counter = new Counter();

// Middleware
app.use(endpoint_mw);

// Endpoints
app.get('', (req, res) => res.send("Welcome!"));
// GET /counter-increase
app.get('/counter-increase', (req, res) => {
    logger.info("[ENDPOINT] GET /counter-increase");
    counter.increase();
    res.send(`${counter.read()}`);
});
// GET /counter-read
app.get('/counter-read', (req, res) => {
    logger.info("[ENDPOINT] GET /counter-read");
    res.send(`${counter.read()}`);
});
// GET /counter-zero
app.get('/counter-zero', (req, res) => {
    logger.info("[ENDPOINT] GET /counter-zero");
    // Zero counter count
    counter.zero();
    res.send(`${counter.read()}`);
});

app.all('*', (req, res) => {
    logger.info(`[ENDPOINT] ${req.method} ${req.originalUrl} - Resource not found.`);
    res.status(404).send("Resource not found.");
});

export default app;