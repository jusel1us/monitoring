class Counter {
    constructor(initialValue = 0, maxValue = Number.MAX_SAFE_INTEGER) {
        this.value = initialValue;
        this.maxValue = maxValue;
    }

    increase() {
        if (this.value < this.maxValue) {
            this.value++;
        } else {
            throw new Error('Counter has reached its maximum value');
        }
    }

    zero() {
        this.value = 0;
    }

    read() {
        return this.value;
    }
}

export default Counter;